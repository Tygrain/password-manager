import React from 'react';
import { View, Clipboard } from 'react-native';
import { List, IconButton, Button, Snackbar } from 'react-native-paper';

export const DataView = ({ navigation, item, array, onClose }) => {
  const [visibleSnackbar, setVisibleSnackbar] = React.useState(false);
  const onToggleSnackBar = () => setVisibleSnackbar(!visibleSnackbar);
  const onDismissSnackBar = () => setVisibleSnackbar(false);

  const copyToBuffer = (text) => {
    Clipboard.setString(text);
    onToggleSnackBar();
  };

  const iconService = (props) => (<List.Icon {...props} icon="web" />);
  const iconAccount = (props) => (<List.Icon {...props} icon="account" />);
  const iconKey = (props) => (<List.Icon {...props} icon="key" />);



  return (
    <View>
      <Button icon="pencil" mode="contained"
        onPress={() => {
          navigation.navigate('Form', { item: item, array: array, edit: true });
          onClose();
        }}>Редактировать</Button>
      <List.Item title="Сервис"
        description={item.service}
        left={iconService}
      />
      <List.Item title="Логин"
        description={item.login}
        left={iconAccount}
        right={(props) => (<IconButton {...props} icon="content-copy" onPress={() => copyToBuffer(item.login)} />)}
      />
      <List.Item title="Пароль"
        description={item.password}
        left={iconKey}
        right={(props) => (<IconButton {...props} icon="content-copy" onPress={() => copyToBuffer(item.password)} />)}
      />
      <Snackbar visible={visibleSnackbar}
        onDismiss={onDismissSnackBar}
        duration={1500}>Скопировано в буффер обмена.</Snackbar>
    </View>
  );
}