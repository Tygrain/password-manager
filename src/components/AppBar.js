import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, BackHandler, Alert } from 'react-native';
import { Appbar, Menu, Divider } from 'react-native-paper';
import { displayName } from '../../app.json';
import { clearAllData } from '../FileStorageUtills';
import { Context } from '../Context';
// import {GDrive} from '../GDrive';

export const AppBar = ({ navigation, previous, scene }) => {
  const [visibleMenu, setVisibleMenu] = React.useState(false);
  const { setAuth } = React.useContext(Context);
  const openMenu = () => setVisibleMenu(true);
  const closeMenu = () => setVisibleMenu(false);
  const title = scene.descriptor.options.title;
  const exportData = () => { };
  const clearData = () => {
    Alert.alert(
      "Подтверждение", "Пароль, и все данные будут сброшены",
      [
        {
          text: "Отмена",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK", onPress: () => {
            clearAllData().then(() => {
              console.log('Data is cleared!');
              setAuth(false);
              // BackHandler.exitApp();
            });
          }
        }
      ]
    );
  };


  return (
    <Appbar.Header>
      <StatusBar />
      {previous ? (<Appbar.BackAction onPress={navigation.goBack} />) : null}
      <Appbar.Content title={displayName}
        subtitle={title}
      />
      {!previous ? (
        <Menu
          visible={visibleMenu}
          onDismiss={closeMenu}
          anchor={<Appbar.Action color="white" icon="dots-vertical" onPress={openMenu} />}
        >
          <Menu.Item onPress={() => { }} title="Импортировать" disabled />
          <Menu.Item onPress={() => { }} title="Экспортировать" disabled />
          <Divider />
          <Menu.Item onPress={clearData} title="Сбросить все" />
        </Menu>
      ) : null}
    </Appbar.Header>
  );
};

const styles = StyleSheet.create({
  container: {
    // marginTop: '3%'
  }
});

