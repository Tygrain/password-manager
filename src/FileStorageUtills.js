import AsyncStorage from '@react-native-async-storage/async-storage';
// import * as FileSystem from 'expo-file-system';

export const getData = async (key) => {
  const value = await AsyncStorage.getItem(key);
  if (!!!value)
    throw new Error('oops');
  return !!value? value: null;
};

export const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    // saving error
  }
};

export const getJsonData = async (key) => {
  const jsonValue = await AsyncStorage.getItem(key);
  if (!!!jsonValue)
    throw new Error('oops');

  return !!jsonValue ? JSON.parse(jsonValue) : null;
};

export const storeJsonData = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    // saving error
    console.log(e);
  }
};

export const clearAllData = async () => {
  try {
    await AsyncStorage.clear();
  } catch(e) {
    // clear error
  }
};