import React from 'react';
import { StyleSheet, View } from 'react-native';
import { TextInput, HelperText, Button } from 'react-native-paper';
import { storeJsonData } from '../FileStorageUtills';
import { passwordGenerator } from '../Generator';

export const FormScreen = ({ navigation, route }) => {
  const editMode = !!route.params.item;
  const [element, setElement] = React.useState(editMode ? route.params.item : {
    service: '',
    login: '',
    password: ''
  });
  const [errorMessage, showErrorMessage] = React.useState(false);
  const hasServiceErrors = value => (errorMessage && !!value) && !(/^[a-zA-Z0-9:@&#\/._-]{4,64}$/.test(value));
  const hasLoginErrors = value => (errorMessage && !!value) && !(/^[a-zA-Z0-9@._-]{4,20}$/.test(value));
  const hasPasswordErrors = value => (errorMessage && !!value) && !(/^[a-zA-Z0-9_-]{6,16}$/.test(value));

  const save = () => {
    if (editMode) {
      for (let i = 0; i < route.params.array.length; i++) {
        if (route.params.array[i] === route.params.item) {
          route.params.array[i] = element;
          break;
        }
      }
    } else {
      route.params.array.push(element);
    }
    storeJsonData('@data', {
      array: route.params.array
    }).then(() => {
      navigation.navigate('Home');
    });
  };

  React.useEffect(() => {
    navigation.setOptions({ title: editMode? 'Редактировать': 'Добавить' });
  }, []);

  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

  React.useEffect(() => {
    let cleanupFunction = false;
    if (!!element.service || !!element.login || !!element.password) {
      sleep(3000).then(() => {
        if(!cleanupFunction) showErrorMessage(!errorMessage);
      });
    }
    return () => cleanupFunction = true;
  }, [element]);

  return (
    <View style={styles.container}>
        <TextInput
          label="Сервис"
          value={element.service}
          autoCapitalize="none"
          autoCompleteType="off"
          autoCorrect={false}
          error={hasLoginErrors(element.service)}
          onChangeText={text => setElement({
            ...element,
            service: text
          })} />
        <HelperText type="error" visible={hasServiceErrors(element.service)}>
          Не верный ввод, поле может содержать только a-z A-Z 0-9 @?&#, точку, двоеточие, косую черту, символ тире, и нижнего подчеркивания.
        </HelperText>
        <TextInput
          label="Логин"
          value={element.login}
          autoCapitalize="none"
          autoCompleteType="email"
          autoCorrect={false}
          error={hasLoginErrors(element.login)}
          onChangeText={text => setElement({
            ...element,
            login: text
          })} />
        <HelperText type="error" visible={hasLoginErrors(element.login)}>
          Не верный ввод, поле может содержать только @ a-z A-Z 0-9, символ тире и нижнего подчеркивания.
        </HelperText>
        <TextInput
          label="Пароль"
          value={element.password}
          autoCapitalize="none"
          autoCompleteType="password"
          autoCorrect={false}
          error={hasLoginErrors(element.password)}
          onChangeText={text => setElement({
            ...element,
            password: text
          })} />
        <HelperText type="error" visible={hasPasswordErrors(element.password)}>
          Не верный ввод, поле может содержать только a-z A-Z 0-9, символ тире и нижнего подчеркивания.
        </HelperText>
        <View style={styles.buttons}>
        <Button mode="contained"
          uppercase={false}
          disabled={errorMessage}
          onPress={save}>Сохранить</Button>
          <Button mode="text"
          uppercase={false}
          onPress={() => setElement({
            ...element,
            password: passwordGenerator(8)
          })}>Сгенерировать</Button>
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0,
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  form: {
  },
  buttons: {
    // flex: 3,
    marginHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});