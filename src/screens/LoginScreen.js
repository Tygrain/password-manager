import React from 'react';
import { View, StyleSheet } from 'react-native';
import { TextInput, Button, HelperText, Avatar } from 'react-native-paper';
import { storeData } from '../FileStorageUtills';
import { Context } from '../Context';

export const LoginScreen = ({ navigation }) => {
  const { setAuth, password } = React.useContext(Context);
  const regMode = password === '0000';
  const [passwordInput, setPasswordInput] = React.useState('');
  const [passwordInput2, setPasswordInput2] = React.useState('');
  const [errorMessage, showErrorMessage] = React.useState(false);

  const hasInputErrors = value => (errorMessage && !!value) && !(/^[a-zA-Z0-9_-]{6,16}$/.test(value));
  const check = () => {
    if (regMode && passwordInput !== '') {
      return (passwordInput == passwordInput2);
    }
    return passwordInput === password;
  };

  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

  React.useEffect(() => {
    navigation.setOptions({ title: regMode? 'Первый вход': 'Вход' });
  }, []);

  React.useEffect(() => {
    let cleanupFunction = false;
    if(!!passwordInput || !!passwordInput2) {
      sleep(3000).then(() => {
        if(!cleanupFunction) showErrorMessage(!errorMessage);
      });
    }
    if (!regMode && check()) setAuth(true);
    return () => cleanupFunction = true;
  }, [passwordInput, passwordInput2]);

  return regMode? (
    <View style={styles.container}>
      <Avatar.Icon style={styles.logo} size={100} icon="key" />
      <View style={styles.form}>
        <TextInput
          label="Пароль"
          autoCapitalize="none"
          autoCompleteType="password"
          autoCorrect={false}
          secureTextEntry
          error={hasInputErrors(passwordInput)}
          onChangeText={setPasswordInput} />
        <HelperText type="error" visible={hasInputErrors(passwordInput)}>
          Не верный ввод, поле может содержать только a-z A-Z 0-9, символ тире и нижнего подчеркивания.
        </HelperText>
        <TextInput
          label="Повторите пароль"
          value={passwordInput2}
          autoCapitalize="none"
          autoCompleteType="password"
          autoCorrect={false}
          secureTextEntry
          error={hasInputErrors(passwordInput2)}
          onChangeText={setPasswordInput2} />
        <HelperText type="error" visible={hasInputErrors(passwordInput2)}>
          Не верный ввод, поле может содержать только a-z A-Z 0-9, символ тире и нижнего подчеркивания.
        </HelperText>
        <Button mode="contained"
          uppercase={false}
          onPress={() => {
            storeData('@password', passwordInput).then(() => {
              setAuth(true);
            });
          }}
          disabled={!check()}>Сохранить пароль</Button>
      </View>
    </View>
  ): (
      <View style={styles.container}>
        <Avatar.Icon style={styles.logo} size={100} icon="key" />
        <View style={styles.form}>
          <TextInput
            label="Пароль"
            value={passwordInput}
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry
            error={hasInputErrors(passwordInput)}
            onChangeText={setPasswordInput} />
          <Button mode="contained"
            uppercase={false}
            onPress={() => check() && setAuth(true)}
            disabled={!check()}>Вход</Button>
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: '10%'
  },
  form: {
    marginVertical: '5%',
    width: '75%',
  }
});