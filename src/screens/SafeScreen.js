import React from 'react';
import { SafeAreaView, StyleSheet, BackHandler } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { useFocusEffect } from '@react-navigation/native';
import { List, FAB, Text, Portal, Modal, Searchbar } from 'react-native-paper';
import { DataView } from '../components/DataView';
import { getJsonData, storeJsonData } from '../FileStorageUtills';
import { array } from '../../defaults.json';

let currentItem = {
  service: '',
  login: '',
  password: ''
};

export const SafeScreen = ({ navigation, route }) => {

  const [searchQuery, setSearchQuery] = React.useState('');
  const [visibleDataModal, setVisibleDataModal] = React.useState(false);
  const showDataModal = (element) => {
    currentItem = element;
    setVisibleDataModal(true);
  };
  const hideDataModal = () => setVisibleDataModal(false);

  const [state, setState] = React.useState({
    data: array,
    selectMode: false,
    selectedCount: 0
  });

  const toggleSelectMode = () => {
    setState(state.selectMode? {
      data: state.data.filter((item) => delete item.selected),
      selectMode: !state.selectMode,
      selectedCount: 0
    }: {
      ...state,
      selectMode: !state.selectMode
    });
  };

  const iconKey = (props) => (<List.Icon {...props} icon="key-variant" />);
  const renderItem = ({ item }) => (
    <List.Item
      left={iconKey}
      title={item.service}
      description={item.login}
      style={item.selected ? styles.selected : {}}
      onPress={() => selectItem(item)}
      onLongPress={() => selectItem(item, true)} />
  );

  const selectItem = (item, longPress = false) => {
    if (state.selectMode || longPress) {
      item.selected = !item.selected;
      setState({
        ...state,
        selectMode: longPress? !state.selectMode: state.selectMode,
        selectedCount: item.selected ? state.selectedCount + 1 : state.selectedCount - 1
      });
    } else {
      showDataModal(item);
    }
    // if (longPress) toggleSelectMode();
    // console.log(state.data);
  };

  const searchItems = () => {
    const query = new RegExp(searchQuery.replace(/[^a-zA-Z@]/g,""), 'i');
    return state.data.filter(item => query.test(item.service) || query.test(item.login));
  };

  const deleteItems = () => {
    const arr = state.data.filter(item => !('selected' in item) || !item.selected);
    storeJsonData('@data', {
      array: arr
    }).then(() => {
      console.log('Deleted items');
      setState({
        data: arr,
        selectMode: !state.selectMode,
        selectedCount: 0
      });
    });
  };


  useFocusEffect(
    React.useCallback(() => {
      getJsonData('@data').then(res => { // Тянем данные с локального хранилища
        console.log('Fetched data');
        setState({
          ...state,
          data: res.array
        });
      }).catch(e => {
        // Данных нет
      });
    }, [])
  );

  React.useEffect(() => {
    navigation.setOptions({ title: state.selectMode? 'Выбрать': null });
  }, [state.selectMode]);

  BackHandler.addEventListener('hardwareBackPress', () => {
    if (route.name === 'Home') {
      if (state.selectMode) {
        console.log('Cброс выделения');
        toggleSelectMode();
      } else {
        console.log('Выход из приложения');
        BackHandler.exitApp();
      }
      return true;
    }
  });

  return (
    <SafeAreaView style={styles.contaiber}>
      <Portal>
        <Modal visible={visibleDataModal} onDismiss={hideDataModal} contentContainerStyle={styles.modal}>
          <DataView navigation={navigation} item={currentItem} array={state.data} onClose={hideDataModal} />
        </Modal>
      </Portal>
      <Searchbar
        placeholder="Найти..."
        onChangeText={setSearchQuery}
        value={searchQuery}
      />
      {!!state.data.length ? (
        <FlatList
          data={!!searchQuery? searchItems(): state.data}
          renderItem={renderItem}
          keyExtractor={(item, id) => id.toString()}
        />) : (
          <Text>Данных нет</Text>
        )}

      {state.selectMode ? (
        <FAB style={styles.fab_delete}
          icon="delete"
          label={state.selectedCount.toString()}
          onPress={deleteItems}
          onLongPress={toggleSelectMode}
        />
      ) : (
          <FAB style={styles.fab_add}
            icon="plus"
            onPress={() => navigation.navigate('Form', { array: state.data })}
          />
        )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  contaiber: {
    height: '100%'
  },
  modal: {
    backgroundColor: 'white',
    margin: 20,
    padding: 20,
    borderRadius: 5
  },
  fab_add: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0
  },
  fab_delete: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: '#F44336'
  },
  selected: {
    backgroundColor: '#BDBDBD'
  }
});