import React from 'react';
import { LoginScreen } from './LoginScreen';
import { SafeScreen } from './SafeScreen';
import { Context } from '../Context';

export const HomeScreen = ({ navigation, route }) => {
  const { auth } = React.useContext(Context);

  return auth ? (<SafeScreen navigation={navigation} route={route} />) :
    (<LoginScreen navigation={navigation} />);
};
