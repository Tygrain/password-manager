
export const randInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

export const passwordGenerator = (length, symbols = "") => {
  const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const numbers = "0123456789";
  let tmp = symbols.concat(letters)
    .concat(letters.toLowerCase())
    .concat(numbers);
  let pass = "";
  for (let i = 0; i < length; i++)
    pass += tmp[randInt(0, tmp.length)];

  return pass;
}