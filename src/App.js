import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ActivityIndicator } from 'react-native';
import { AppBar } from './components/AppBar';
import { HomeScreen } from './screens/HomeScreen';
import { FormScreen } from './screens/FormScreen';
import { getData } from './FileStorageUtills';
import { Context } from './Context';

const Stack = createStackNavigator();

export default function App() {
  const [password, setPassword] = React.useState();
  const [auth, setAuth] = React.useState(false);

  React.useEffect(() => {
    getData('@password').then(res => {
      console.log('Password: ', res);
      setPassword(res);
    }).catch(e => {
      setPassword('0000');
    });
  }, []);

  return !!password ? (
    <Context.Provider value={{
      auth, setAuth, password
    }}>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{
          header: AppBar
        }}>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            initialParams={{ password: password }}
          />
          <Stack.Screen
            name="Form"
            component={FormScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Context.Provider>
  ) : (
    <ActivityIndicator animating={true} size="large" />
  );

}